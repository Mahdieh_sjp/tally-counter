package sbu.cs;

public class TallyCounter implements TallyCounterInterface {

    private static int total;

    @Override
    public void count() {
        if (total  < 9999)
            total++;
    }

    @Override
    public int getValue() {
        return total;
    }

    @Override
    public void setValue(int value) throws IllegalValueException {
        if (value < 0 || value > 9999)
            throw new IllegalValueException();

        total = value;
    }
}
