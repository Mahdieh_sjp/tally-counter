package sbu.cs;

import java.util.Random;

public class ExerciseLecture5 {


    public String weakPassword(int length) {
        StringBuilder password = new StringBuilder();
        Random random = new Random();

        for(int i = 0; i < length; i++)
            password.append((char)( random.nextInt(26) + 97));
        System.out.println(password.toString());
        return password.toString();
    }


    public String strongPassword(int length) throws Exception {
        if (length < 3)
            throw new Exception();

        char[] password = new char[length];
        Random random = new Random();

        password[0] = generateSpecialCharacter();
        password[1] = generateDigit();
        password[2] = generateLetter();

        for(int i = 3; i < length; i++){
            int j = random.nextInt(3);
            if(j == 0)
                password[i] = generateLetter();
            else if(j == 1)
                password[i] = generateDigit();
            else if(j == 2)
                password[i] = generateSpecialCharacter();
        }


        return new String(shuffle(password));
    }


    private char generateSpecialCharacter(){
        Random random = new Random();
        char[] specialCharacters =  "\"!#$%&' ()*+,-./:;<=>?@[]^_` {|}~".toCharArray();
        int index = random.nextInt(specialCharacters.length);
        return specialCharacters[index];
    }


    private char generateDigit() {
        Random random = new Random();
        return (char)(random.nextInt(10) + '0');
    }


    private char generateLetter(){
        Random random = new Random();
        int ch;

        do {
            ch = random.nextInt(58);
        }while(ch > 25 && ch < 32);

        ch += 'A';
        return (char) ch;
    }


    private char[] shuffle(char[] str){
        Random random = new Random();

        for (int i = 0;i < str.length; i++){
            int j = random.nextInt(str.length);
            char temp = str[i];
            str[i] = str[j];
            str[j] = temp;
        }
        return str;
    }


    public boolean isFiboBin(int n) {
        int i = 1;

        while(true) {
            if(n == (int) fibonacci(i)
                    + Integer.toBinaryString((int)fibonacci(i)).replace("0","").length())
                return true;
            if(n < (int) fibonacci(i))
                return false;
            i++;
        }

    }


    public long fibonacci(int n) {
        long result = 1, n1 = 0, n2 = 1;

        for(int i = 1; i < n; i++){
            {
                result = n1 + n2;
                n1 = n2;
                n2 = result;
            }
        }
        return result;
    }

}
