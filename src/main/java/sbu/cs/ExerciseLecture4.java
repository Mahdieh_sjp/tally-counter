package sbu.cs;

public class ExerciseLecture4 {


    public long factorial(int n) {
        int result = 1;

        for (int i = 1; i <= n; i++)
            result *= i;

        return result;
    }


    public long fibonacci(int n) {
        long result = 1, n1 = 0, n2 = 1;

        for(int i = 1; i < n; i++){
            {
                result = n1 + n2;
                n1 = n2;
                n2 = result;
            }
        }
        return result;
    }


    public String reverse(String word) {
        StringBuilder result = new StringBuilder();

        for(int i = word.length() - 1; i >= 0; i--)
            result.append(word.charAt(i));
        return result.toString();
    }


    public boolean isPalindrome(String line) {
        String rev = "";

        for ( int i = line.length() - 1; i >= 0; i-- )
            if (line.charAt(i) != ' ')
                rev += line.charAt(i);

        if(rev.toLowerCase().equals(line.toLowerCase().replaceAll(" ","")))
            return true;

        return false;
    }


    public char[][] dotPlot(String str1, String str2) {
        char[][] dots = new char[str1.length()][str2.length()];

        for(int i = 0; i < str1.length(); i++) {
            for (int j = 0; j < str2.length(); j++) {
                if (str1.charAt(i) == str2.charAt(j))
                    dots[i][j] = '*';

                else dots[i][j] = ' ';
            }
        }

        return dots;
    }
}
