package sbu.cs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ExerciseLecture6 {


    public long calculateEvenSum(int[] arr) {
        long result = 0;
        for (int i = 0; i < arr.length; i += 2)
            result += arr[i];


        return result;
    }


    public int[] reverseArray(int[] arr) {
        int[] reversedArray = new int[arr.length];

        for (int i = arr.length - 1; i >= 0; i--){
            reversedArray[arr.length - 1 - i] = arr[i];
        }
        return reversedArray;
    }


    public double[][] matrixProduct(double[][] m1, double[][] m2)
            throws RuntimeException {
        double[][] product = new double[m1.length][m2[0].length];

        for (int i = 0; i < m1.length; i++) {
            for (int j = 0; j < m2[0].length; j++) {
                product[i][j] = productOfRowColumn(m1, m2, i, j);
            }
        }

        return product;
    }

    public static double productOfRowColumn(double[][] rowM1, double[][] columnM2, int row, int column) {
        double result = 0;

        for (int i = 0; i < rowM1[0].length; i++)
            result += rowM1[row][i] * columnM2[i][column];

        return result;
    }


    public List<List<String>> arrayToList(String[][] names) {
        List<List<String>> result = new ArrayList<>();

        for (int i = 0; i < names.length; i++){
            List<String> row = new ArrayList<>();

            for (int j = 0; j < names[i].length; j++)
                row.add(names[i][j]);

            result.add(row);
        }
        return result;
    }


    public List<Integer> primeFactors(int n) {
        List<Integer> primesSmallerThanNumber = primesSmallerThanNumber(n);
        ArrayList<Integer> primeFactors = new ArrayList<>();

        for(Integer prime: primesSmallerThanNumber)
            if (n % prime == 0)
                primeFactors.add(prime);

        return primeFactors;
    }

    private static List<Integer> primesSmallerThanNumber(int number) {
        List<Integer> primes = new ArrayList<>();

        if (number < 2)
            return primes;

        primes.add(2);

        if (number < 3)
            return primes;

        for (int i = 3; i < number / 2; i++) {
            boolean isPrime = true;

            for (Integer prime : primes)
                isPrime = isPrime && (i % prime != 0);

            if (isPrime)
                primes.add(i);

        }
        boolean isPrime = true;

        for (Integer prime : primes)
            isPrime = isPrime && number % prime != 0;

        if (isPrime)
            primes.add(number);

        return primes;
    }


    public List<String> extractWord(String line) {
        List<String> result = new ArrayList<>();

        String[] words = line.replace("!", "")
                .replaceAll("\\.", "")
                .replaceAll(",","")
                .replaceAll("\\?","")
                .split(" ");

        return Arrays.asList(words);
    }
}
